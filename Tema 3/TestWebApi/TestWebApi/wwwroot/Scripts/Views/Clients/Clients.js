class Clients
{
    #clientsService = null;
    //this.Http = null;
    Clients = [];

    constructor(clientsService)
    {
        //this.Http = $http;
        this.#clientsService = clientsService;
        this.GetAll();
    }

    AddClient()
    {
    }

    GetAll()
    {
        //this.Http(
        //{
        //    method: 'GET',            
        //    url: 'api/clients'            
        //})
        this.#clientsService.GetAll().then(
            (response) => this.LoadClients(response.data),
            (e) => alert(e.statusText));
    }

    LoadClients(clients)
    {
        this.Clients = clients;
    }
}

Clients.$inject = ['ClientsService'];

LibraryApp.
  component('clients', {   
    templateUrl: 'scripts/views/clients/clients.html',
    controller: Clients,
    controllerAs: "vm"
  });
class Books
{
  #location = null;

  #items = [];
  get Items()
  {
    return this.#items;
  }
  set Items(value)
  {
    this.#items.length = 0;

    for(var i in value)
      this.#items.push(value[i]);
  }

  GridOptions = 
  {
      columnDefs : 
      [
        { name: 'Actions', cellEditableCondition: false, cellTemplate: '<i ng-click="grid.appScope.Edit(row.entity)" class="fs-5 bi bi-pencil-square"></i><i ng-click="grid.appScope.Display(row.entity)" class="fs-5 bi bi-play"></i><i ng-click="grid.appScope.Delete(row.entity)" class="fs-5 bi bi-trash"></i>'},
        { name: 'Code', field: 'Code'},
        { name: 'Title', field: 'Title'},
        { name: 'Author', field: 'Author'},
        { name: 'Year', field: 'Year'},
      ],
      data : this.Items,
      appScopeProvider : this
    };

  constructor($location, booksService)
  {
    this.GetAll(booksService);
    this.#location = $location;
  }

  GetAll(booksService)
  {
    booksService.GetAll().then(
        (items)=> this.LoadBooks(items)), 
        (error)=> alert(error);
  }

  LoadBooks(books)
  {
    // Datos de prueba, cuando venga del servidor books vendrá lleno
    if (books === null)
    {  
      
    }
 
    this.Items = books;
  }

  Edit(book)
  {
    alert("Edit " + book.Title);
  }

  Display(book)
  {
    this.#location.path("/Books/Display/" + book.Id)
    //this.#location.path("/books/display");
  }

  Delete(book)
  {
    alert("Delete " + book.Title);
  }

}

Books.$inject = ['$location', 'BooksService'];

App.
  component('books', {   
    templateUrl: 'Scripts/Views/NavigationViews/BookViews/Books/Books.html',
    controller: Books,
    controllerAs: "vm"
  });
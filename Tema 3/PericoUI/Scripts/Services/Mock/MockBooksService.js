class MockBooksService
{
    static Items = new Map();
    static ItemsByCode = new Map();

    constructor()
    {
        if (MockBooksService.Items.size == 0)
        {
            var book1 = new Book();
            book1.Id = Guid.NewGuid();
            book1.Code = "a1";
            book1.Title = "The Lord of the rings";
            book1.Author = "JRR Tolkien";
            book1.Year = 1954;

            var book2 = new Book();
            book2.Id = Guid.NewGuid();
            book2.Code = "a2";
            book2.Title = "Dune";
            book2.Author = "Frank Herbert";
            book2.Year = 1965;

            MockBooksService.Items.set(book1.Id, book1);
            MockBooksService.Items.set(book2.Id, book2);

            MockBooksService.ItemsByCode.set(book1.Code, book1);
            MockBooksService.ItemsByCode.set(book2.Code, book2);
        }
    }

    Add(item)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var existingBook = MockBooksService.ItemsByCode.get(item.Code);
            if (Tools.IsNullOrUndefined(existingBook))
            {
                item.Id = Guid.NewGuid();
                MockBooksService.Items.set(item.Id, item);
                MockBooksService.ItemsByCode.set(item.Code, item);

                onSuccess(true);
            }
            else
            {
                onError("ya existe un libro con el código:" + item.Code);
            }
            
        });

        return myPromise;
    }

    Update(item)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var bookToUpdate = MockBooksService.Items.get(item.Id);
            if (!Tools.IsNullOrUndefined(bookToUpdate))
            {
                if (bookToUpdate.Code != item.Code)
                {                   
                    var existingCode = MockBooksService.ItemsByCode.get(item.Code);
                    if (Tools.IsNullOrUndefined(existingCode))
                    {
                        MockBooksService.ItemsByCode.delete(item.Code);
                        bookToUpdate.Code = item.Code;                    
                        MockBooksService.ItemsByCode.set(item.Code, item);
                    }
                    else
                    {
                        onError("ya existe un libro con el código:" + item.Code);
                        return;
                    }

                }

                bookToUpdate.Title = item.Title;                    
                bookToUpdate.Author = item.Author;                    
                bookToUpdate.Year = item.Year;                    

                onSuccess(true);
            }
            else
            {
                onError("no existe ningún libro con ese id:" + item.Id);
            }
            
        });

        return myPromise;
    }

    GetAll()
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = Array.from(MockBooksService.Items, ([name, value]) => value);
            onSuccess(output);
        });

        return myPromise;
    }

    Find(id)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var output = MockBooksService.Items.get(id);
            
            if(output !== null)
                onSuccess(output);
            else
                onError("book not found for id " + id);
        });

        return myPromise;
    }

    Delete(id)
    {
        let myPromise = new Promise(function(onSuccess, onError) 
        {
            var itemToDelete = MockBooksService.Items.get(id);
            
            if (!Tools.IsNullOrUndefined(itemToDelete))
            {
                MockBooksService.Items.delete(id);
                onSuccess(true);
            }
            else
                onError("error deleting: book not found for id " + id);
        });

        return myPromise;
    }
}


App.service('BooksService', MockBooksService);
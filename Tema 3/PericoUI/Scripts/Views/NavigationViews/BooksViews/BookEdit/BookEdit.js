class BookEdit
{
  #location = null;
  #booksService = null;
  #scope = null;

  Book = null;

  Code = "";
  Title = "";
  Author = "";
  Year = "";

  constructor(routeParams, location, booksService, scope)
  {
    let id = routeParams.id;
    this.#location = location;
    this.#booksService = booksService;  
    this.#scope = scope;
    this.Find(id);
  }

  Find(id)
  {
    this.#booksService.Find(id).then(
      (item) => 
      {
         this.Book = item.Clone();
         this.Code = item.Code;
         this.Title = item.Title;
         this.Author = item.Author;
         this.Year = item.Year;
         this.#scope.$apply();
        }, 
      (error) => this.OnFindError(error));
  }

  Update()
  {
    this.Book.Code = this.Code;
    this.Book.Title = this.Title;
    this.Book.Author = this.Author;
    this.Book.Year = this.Year; 

    this.#booksService.Update(this.Book).then(
      (item) => this.OnUpdateSuccess(item), 
      (error) => this.OnUpdateError(error));
  }

  OnUpdateSuccess(result)
  {
    if (result)
      this.#location.path("/Books");
    else
      alert("no se ha podido guardar");
  }

  OnUpdateError(error)
  {
    alert(error);
  }

}

BookEdit.$inject = ['$routeParams','$location', 'BooksService', '$scope'];

App.
  component('bookedit', {   
    templateUrl: 'scripts/views/NavigationViews/BooksViews/bookedit/bookedit.html',
    controller: BookEdit,
    controllerAs: "vm"
  });
class BookDisplay
{
  #booksService = null;
  #scope = null;

  Code = "";
  Title = "";
  Author = "";
  Year = "";

  constructor(routeParams, booksService, scope)
  {
    var id = routeParams.id;
    this.#booksService = booksService;  
    this.#scope = scope;  

    this.Find(id);
  }

  Find(id)
  {
    this.#booksService.Find(id).then(
      (item) => this.OnFindSuccess(item)), 
      (error) => this.OnFindError(error);
  }

  OnFindSuccess(item)
  {
    this.Code = item.Code;
    this.Title = item.Title;
    this.Author = item.Author;
    this.Year = item.Year;

    this.#scope.$apply();
  }

  OnFindError(error)
  {
    alert(error);
  }

}

BookDisplay.$inject = ['$routeParams','BooksService','$scope',];

App.
  component('bookdisplay', {   
    templateUrl: 'scripts/views/NavigationViews/BooksViews/bookdisplay/bookdisplay.html',
    controller: BookDisplay,
    controllerAs: "vm"
  });
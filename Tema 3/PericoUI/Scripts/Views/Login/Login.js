class Login
{
  Email = "";
  Password = "";
  LoginService = null;
  Scope = null;

  constructor($scope, loginService)
  {
    this.Scope = $scope;
    this.LoginService = loginService;
  }

  RequestLogin()
  {
    this.LoginService
          .RequestLogin(this.Email, this.Password)
          .then( 
            user => 
            {
              App.ClientGlobals.CurrentUser = user;             
              this.Scope.$apply();
            },
            error => alert(error));
  }
}

Login.$inject = ['$scope', 'LoginService'];

App.
  component('login', {   
    templateUrl: 'scripts/views/login/login.html',
    controller: Login,
    controllerAs: "vm"
  });
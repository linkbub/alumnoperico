class Book
{
    Id = "";
    Code = "";
    Title = "";
    Author = "";
    Year = 0;

    constructor()
    {

    }

    Clone()
    {
        let output = new Book();
        
        output.Id = this.Id;
        output.Code = this.Code;
        output.Title = this.Title;
        output.Author = this.Author;
        output.Year = this.Year;

        return output;
    }
}
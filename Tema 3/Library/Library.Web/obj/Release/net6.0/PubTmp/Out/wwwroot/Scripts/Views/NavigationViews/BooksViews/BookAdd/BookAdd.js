class BookAdd
{
  #location = null;
  #booksService = null;

  Code = "";
  Title = "";
  Author = "";
  Year = "";

  constructor(location, booksService)
  {
    this.#location = location;
    this.#booksService = booksService;  
  }

  Add()
  {
    var book = new Book();
    book.Code = this.Code;
    book.Title = this.Title;
    book.Author = this.Author;
    book.Year = this.Year; 

    this.#booksService.Add(book).then(
      (item) => this.OnAddSuccess(item), 
      (error) => this.OnAddError(error));
  }

  OnAddSuccess(result)
  {
    if (result)
      this.#location.path("/Books");
    else
      alert("no se ha podido guardar");
  }

  OnAddError(error)
  {
    alert(error);
  }

}

BookAdd.$inject = ['$location', 'BooksService'];

App.
  component('bookadd', {   
    templateUrl: 'scripts/views/NavigationViews/BooksViews/bookadd/bookadd.html',
    controller: BookAdd,
    controllerAs: "vm"
  });
class LoginService
{
    constructor($http)
    {
       this.Http = $http;
    }

    RequestLogin(email, password)
    {
        let url = 'api/login/' + email + "/" + password;
        let self = this;

        return new Promise(function(onSuccess, onError) 
        {
            self.Http(
                {
                    method: 'GET',            
                    url: url     
                })
                .then( 
                    response => 
                    {
                        onSuccess(response.data);
                    },
                    error => onError(error));
        });
           
    }
}

LoginService.$inject = ['$http'];
App.service('LoginService', LoginService);
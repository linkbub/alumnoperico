﻿using Library.Lib.DAL;
using Library.Lib.Models;
using Library.Web.App;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Library.Web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        LibraryDbContext dbContext { get; set; }

        public BooksController()
        {
            var contextFactory = new LibraryDbContextFactory();
            dbContext = contextFactory.CreateDbContext();
        }

        // GET: api/<BooksController>
        [HttpGet]
        public IEnumerable<Book> Get()
        {
            if (dbContext.Books.Count() == 0)
            {
                var book1 = new Book();

                book1.Id = Guid.NewGuid();
                book1.Title = "The Lord of the Rings";
                book1.Code = "B0001";
                book1.Year = 1954;
                book1.Author = "JRR Tolkien";

                var book2 = new Book();

                book2.Id = Guid.NewGuid();
                book2.Title = "Dune";
                book2.Code = "B0002";
                book2.Year = 1963;
                book2.Author = "Frank Herbert";

                dbContext.Books.Add(book1);
                dbContext.Books.Add(book2);

                dbContext.SaveChanges();
            }


            return dbContext.Books.ToList();
        }

        // GET api/<BooksController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<BooksController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<BooksController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<BooksController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

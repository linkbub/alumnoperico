class Register
{
    Email = "";
    Password = "";
    RegisterService = null;
    Scope = null;

    constructor($scope, registerService) {
        this.Scope = $scope;
        this.RegisterService = registerService;
    }

    RequestRegister()
    {
        let registerInfo = new RegisterInfo();
        registerInfo.Email = this.Email;
        registerInfo.Name = this.Name;
        registerInfo.Password = this.Password;

        this.RegisterService
            .RequestRegister(registerInfo)
            .then(
                response => {
                    if (response.data.isSuccess)
                    {
                        App.ClientGlobals.CurrentUser = user; 
                    }
                    else 
                    {
                        alert(response.data.message);
                    }
                },
                error => alert(error));
    }
}

Register.$inject = ['$scope', 'RegisterService'];

App.
    component('register', {
        templateUrl: 'scripts/views/register/register.html',
        controller: Register,
        controllerAs: "vm"
    });
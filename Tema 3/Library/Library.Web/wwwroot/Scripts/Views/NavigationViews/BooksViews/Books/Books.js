class Books
{
  #scope = null;
  #location = null;
  #items = [];
  #booksService = null;

  get Items()
  {
    return this.#items;
  }
  set Items(value)
  {
    this.#items.length = 0;

    for(var i in value)
      this.#items.push(value[i]);
  }

  GridOptions = 
  {
      columnDefs : 
      [
        { name: 'Actions', cellEditableCondition: false, cellTemplate: '<i ng-click="grid.appScope.Edit(row.entity)" class="fs-5 bi bi-pencil-square"></i><i ng-click="grid.appScope.Display(row.entity)" class="fs-5 bi bi-play"></i><i ng-click="grid.appScope.Delete(row.entity)" class="fs-5 bi bi-trash"></i>'},
        { name: 'Code', field: 'code'},
        { name: 'Title', field: 'title'},
        { name: 'Author', field: 'author'},
        { name: 'Year', field: 'year'},
      ],
      data : this.Items,
      appScopeProvider : this
    };

  constructor(scope, location, booksService)
  {
    this.#scope = scope;
    this.#location = location;
    this.#booksService = booksService;
    this.GetAll();
  }

  GetAll()
  {
    this.#booksService.GetAll().then(
      (items) => this.OnGetAllSuccess(items), 
      (error) => this.OnGetAllError(error));
  }

  OnGetAllSuccess(items)
  {
    this.Items = items.data;
    //this.#scope.$apply();
  }

  OnGetAllError(error)
  {
    alert(error);
  }

  GotoAdd()
  {
    this.#location.path("/Books/Add");
  }

  Edit(item)
  {
    this.#location.path("/Books/Edit/" + item.Id)
  }

  Display(item)
  {    
    //alert("mostrar " + item.Title);
    this.#location.path("/Books/Display/" + item.Id)
  }

  Delete(item)
  {
    var r = confirm("Va a borrar el libro: " + item.Title + " con código "+ item.Code + ". ¿Está seguro?");
    if (r == true) 
    {
      this.#booksService.Delete(item.Id).then(
        (result) => 
        {
          if(result)
          {
            alert("se ha borrado correctamete el libro");
            this.GetAll();
          }
        }, 
        (error) => alert(error));
    } 
  }

}

Books.$inject = ['$scope', '$location', 'BooksService'];

App.
  component('books', {   
    templateUrl: 'scripts/views/NavigationViews/BooksViews/books/books.html',
    controller: Books,
    controllerAs: "vm"
  });
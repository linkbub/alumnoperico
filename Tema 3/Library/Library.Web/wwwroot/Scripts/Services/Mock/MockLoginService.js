class MockLoginService
{
    static Users = new Map();

    constructor()
    {
        if (MockLoginService.Users.size == 0)
        {
            let member1 = new Member();
            member1.Id = "1";
            member1.Name = "Perico";
            member1.Email = "a@a.com";
            member1.Password = "1234";

            let member2 = new Member();
            member2.Id = "2";
            member2.Name = "Pocholo";
            member2.Email = "b@b.com";
            member2.Password = "4321";

            MockLoginService.Users.set(member1.Id, member1);
            MockLoginService.Users.set(member2.Id, member2);
        }
    }

    RequestLogin(email, password)
    {
        return new Promise(function(onSuccess, onError) 
        {
            let users = MockLoginService.Users.values();
            for (let i = 0; i < MockLoginService.Users.size; i++)
            {
                let user = users.next().value;
                if (user.Email == email && user.Password == password)
                {    
                    onSuccess(user); 
                    return;
                }

                onError("no se ha encontrado un usuario con ese email y password");
            }
        });
    }
}


App.service('LoginService', MockLoginService);